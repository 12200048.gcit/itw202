import React from 'react'
import {View, Image, Text, StyleSheet} from 'react-native';

export default function Img(){
    return(
        <View>
            <View>
            <Image source={{uri: 'https://picsum.photos/100/100'}} style={{width: 100, height: 100 }}/>
            </View>
            
            <View>
            <Image
                 source={require('../assets/logo.png')} style = {{width: 100,
                     height: 100,
              }}/>
            </View>

        </View>
    )
}