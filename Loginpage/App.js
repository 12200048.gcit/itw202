import { NavigationContainer } from '@react-navigation/native';
import { StyleSheet, Text, View, Button } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';


const Stack = createStackNavigator();

function Screen1({navigation}){
  // console.log(navigation)
  return(
    <View style = {styles.container}>
      <Text>Screen1</Text>
      <Button
      title='go to second screen'
      onPress={ () => {
        navigation.navigate('Screen2')
      }}/>
 
      {/* <Button
      title='go to third screen'
      onPress={ () => {
        navigation.reset({
          index: 0,
          routes: [{name: 'Screen3'}]
        })
      }}/> */}
    
      </View>
  )
}
function Screen2({navigation}){
  return(
  <View style={styles.container}>
    <Text>screen2</Text>
    {/* <Button
    title='GO back'
   onPress={ () => {
     navigation.goBack();
   }}
   /> */}
   <Button
   title='go to third screen'
   onPress={() => {
     navigation.replace("Screen3")
   }}/>
  </View>
  )
}
function Screen3({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen3</Text>
      <Button
    title='Go back'
   onPress={ () => {
     navigation.goBack();
   }}
   />
    </View>
  )
}
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
     initialRouteName='Screen1'
      screenOptions={{headerShown: false,}}>
        <Stack.Screen name='Screen1' component={Screen1} />
        <Stack.Screen name='Screen2' component={Screen2}/>
        <Stack.Screen name='Screen3' component={Screen3}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
