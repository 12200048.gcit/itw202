import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.b1}><Text style={styles.t1}>1</Text></View>
      <View style={styles.b2}><Text style={styles.t1}>2</Text></View>
      <View style={styles.b3}><Text style={styles.t1}>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  
    flexDirection: 'row',
    paddingTop: 50,
    paddingLeft: 60
  },
 
  

  b1: {
      height: 300,
      width: 80,
      backgroundColor: 'red',
      justifyContent: 'center',
      alignItems: 'center',

  },

  b2: {
    height: 300,
    width: 150,
    backgroundColor: 'blue',
    justifyContent: 'center',
      alignItems: 'center'
  },
  b3: {
    height: 300,
    width: 10,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
