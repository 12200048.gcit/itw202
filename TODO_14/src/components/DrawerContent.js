import { DrawerContentScrollView } from '@react-navigation/drawer'
import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Text, Avatar, Title, Caption, Paragraph, Drawer, TouchableRipple, Switch } from 'react-native-paper'

export default function DrawerContent(){
    return(
        <DrawerContentScrollView>
            <View style={styles. drawerContent}>
                <View style={styles.userInfoSection}>
                    <Avatar.Image
                    size={80}
                    source={
                        require('../../assets/luffy.jpg')
                    }/>

                    <Title style={styles.title}>Jamyang Gyeltshen</Title>
                    <Caption style={styles.caption}>gcit year 2</Caption>

                    <View style={styles.row}>
                        <View style={styles.section}>
                            <Paragraph style={[styles.paragraph, styles.caption]}>17</Paragraph>
                            <Caption style={styles.caption}>Following</Caption>
                        </View>
                        <View style={styles.section}>
                            <Paragraph style={[styles.paragraph, styles.caption]}>18</Paragraph>
                            <Caption style={styles.caption}>Followers</Caption>
                        </View>
                    </View>
                </View>
            </View>
            <Drawer.Section style={styles.drawerSection}>
                <Drawer.Item
                label='Settings'
                onPress={()=>{}}/>
            </Drawer.Section>

            <Drawer.Section title='preferences'>
                <TouchableRipple onPress={() => {}}>
                    <View style={styles.perference}>
                        <Text>Notifications</Text>
                        <View pointerEvents='none'>
                            <Switch value={false} />
                        </View>
                    </View>

                </TouchableRipple>

            </Drawer.Section>
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20
    },
    title: {
        marginTop: 20,
        fontWeight: 'bold'
    },
    caption: {
        fontSize: 14,
        lineHeight: 14
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 4
    },
    drawerSection: {
        marginTop: 15
    },
    perference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        paddingVertical: 12

    }
})