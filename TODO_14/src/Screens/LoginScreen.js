import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import { Paragraph } from 'react-native-paper'
import Header from '../components/Header'
import Button from '../components/button'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helper/emailValidator'
import { passwordValidator } from '../core/helper/passwordValidator'
import BackButton from '../components/BackButton'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { theme } from '../core/Theme'
import {loginUser} from '../api/auth-api'


export default function LoginScreen({navigation}){
    const [email, setEmail] = useState({value: '', error: ''})
    const [password, setPassword] = useState({value: '', error: ''})
    const [loading, setLoading] = useState();

    

    const onLoginPressed = async() => {
        const emailError = emailValidator(email.value)
        const passwordError = passwordValidator(password.value)
        if (emailError || passwordError) {
            setEmail({...email, error: emailError})
            setPassword({...password, error: passwordError})
        }
    setLoading(true)
    const response = await loginUser ({
        email: email.value,
        password: password.value
    })
    if (response.error){
        alert(response.error)
    }
    // else{
        
    //     navigation.replace('HomeScreen')
    // }
    setLoading(false)
        // else{
        //     navigation.navigate("HomeScreen")
        // }
    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
           <Header>welcome</Header>
           <TextInput
                label='Email'
                value= {email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={( text)=> setEmail({value: text, error: ""})}/>

             <TextInput
                label='Password'
                value= {password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={( text)=> setPassword({value: text, error: ""})}
                secureTextEntry/>

            <View style={styles.forgotpassword}>
               <TouchableOpacity onPress={() => navigation.navigate("ResetPasswordScreen")}>
                   <Text style={styles.forgot}> Forgot your Password</Text>
               </TouchableOpacity>
           </View>
           <Button loading={loading} mode='contained' onPress = {onLoginPressed}>Login</Button>
           <View style={styles.row}>
               <Text>Dont have an account?</Text>
               <TouchableOpacity onPress={() => navigation.replace("RegistrationScreen")}>
                   <Text style={styles.link}> Sign up</Text>
               </TouchableOpacity>
           </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary
    },
    forgotpassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24
    },
    forgot: {
        fontSize: 13,
        color:theme.colors.secondary
    }
})