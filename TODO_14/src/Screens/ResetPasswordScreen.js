import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import { Paragraph } from 'react-native-paper'
import Header from '../components/Header'
import Button from '../components/button'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helper/emailValidator'
import { passwordValidator } from '../core/helper/passwordValidator'
import BackButton from '../components/BackButton'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { theme } from '../core/Theme'
import { sendEmailWithPassword } from '../api/auth-api'
import firebase from 'firebase/app'
import 'firebase/auth'

export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value: '', error: ''})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async () => {
        const emailError = emailValidator(email.value)
        
        if (emailError ) {
            setEmail({...email, error: emailError})
        }
        setLoading(true)
        const response = await sendEmailWithPassword(email.value);
        if (response.error){
            alert(response.error);
        }else{
            alert("email with password has been sent")
        }
        setLoading(false)
    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
           <Header>Restore Password</Header>
           <TextInput
                label='Email'
                value= {email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={( text)=> setEmail({value: text, error: ""})}
                description="You will receive email with the password reset link "/>


           <Button loading = {loading} mode='outlined' onPress = {onSubmitPressed}>Send Instruction</Button>
           
        </Background>
    )
}

const styles = StyleSheet.create({
    
 
})