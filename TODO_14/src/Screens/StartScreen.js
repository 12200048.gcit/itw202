import React from 'react'
import {View, StyleSheet} from 'react-native'
import { Paragraph } from 'react-native-paper'
import Header from '../components/Header'
import Button from '../components/button'
import Background from '../components/Background'
import Logo from '../components/Logo'

export default function StartScreen({navigation}){
    return(
        <Background>
            <Logo/>
           <Header>Login Template</Header>
           <Paragraph>The easiest way to start</Paragraph>
           <Button 
                mode='outlined'
                onPress={() => {
                     navigation.navigate('LoginScreen')
             }}>Login</Button>

           <Button mode='contained'
           onPress={() => {
            navigation.navigate('RegistrationScreen')
             }}>Sign up</Button>
        </Background>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    }
})