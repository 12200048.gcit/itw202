import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import { Paragraph } from 'react-native-paper'
import Header from '../components/Header'
import Button from '../components/button'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/TextInput'
import { emailValidator } from '../core/helper/emailValidator'
import { passwordValidator } from '../core/helper/passwordValidator'
import BackButton from '../components/BackButton'
import { nameValidator } from '../core/helper/nameValidator'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { theme } from '../core/Theme'
import { signUPUser } from '../api/auth-api'

export default function RegistrationScreen({navigation}){
    const [email, setEmail] = useState({value: '', error: ''})
    const [password, setPassword] = useState({value: '', error: ''})
    const [name, setName] = useState({value: '' , error: ''})
    const [loading, setLoading] = useState();

    const onSignupPressed = async() => {
        const emailError = emailValidator(email.value)
        const passwordError = passwordValidator(password.value)
        const nameError = nameValidator(name.value)
        if (emailError || passwordError || nameError) {
            setEmail({...email, error: emailError})
            setPassword({...password, error: passwordError})
            setName({...name, error: nameError})
        }
        setLoading(true)
        const response = await signUPUser ({
            name: name.value,
            email: email.value,
            password: password.value
        })
        if (response.error){
            alert(response.error)
        }
        else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }
        setLoading(false)
    }

    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
           <Header>create account</Header>

           <TextInput
                label='Name'
                value= {name.value}
                error={name.error}
                errorText={name.error}
                onChangeText={( text)=> setName({value: text, error: ""})}/>

           <TextInput
                label='Email'
                value= {email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={( text)=> setEmail({value: text, error: ""})}/>

             <TextInput
                label='Password'
                value= {password.value}
                error={password.error}
                errorText={password.error}
                onChangeText={( text)=> setPassword({value: text, error: ""})}
                secureTextEntry/>

           <Button loading={loading} mode='contained' onPress = {onSignupPressed}>Sign up</Button>
           <View style={styles.row}>
               <Text>Already have an account?</Text>
               <TouchableOpacity onPress={() => navigation.replace("LoginScreen")}>
                   <Text style={styles.link}> Login</Text>
               </TouchableOpacity>
           </View>
        </Background>
    )
}

const styles = StyleSheet.create({
    
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary
    }
})