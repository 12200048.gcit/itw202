import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image} from 'react-native';
import {  Provider } from 'react-native-paper';
import { theme } from './src/core/Theme';
import Button from './src/components/button';
import TextInput from './src/components/TextInput';
import Header from './src/components/Header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StartScreen, LoginScreen, } from './src/Screens';
import RegistrationScreen from './src/Screens/RegistrationScreen';
import { ResetPasswordScreen } from './src/Screens';
import { HomeScreen } from './src/Screens';
import { ProfileScreen } from './src/Screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerContent from './src/components/DrawerContent';
import  firebase from 'firebase/app';
import { firebaseConfig } from './src/core/Config';
import { AuthLoadingScreen } from './src/Screens';


if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return(
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
        initialRouteName='AuthLoadingScreen'
        screenOptions={{headerShown: false}}>
          <Stack.Screen name = 'StartScreen' component={StartScreen}/>
          <Stack.Screen name = 'LoginScreen' component={LoginScreen}/>
          <Stack.Screen name = 'RegistrationScreen' component={RegistrationScreen}/>
          <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>
          <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>
          <Stack.Screen name='AuthLoadingScreen' component={AuthLoadingScreen}/>
          
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
function BottomNavigation(){
    return(
      <Tab.Navigator>
          <Tab.Screen name='Home' component={HomeScreen}
          options={{
            tabBarIcon: ({size}) => {
             return(
               <Image
               style={{width: size, height: size}}
               source={
                 require('./assets/Home-icon.png')
               }/>
             )
            }
          }}/>

          <Tab.Screen name='Profile' component={ProfileScreen}
          options={{
            tabBarIcon: ({size}) => {
             return(
               <Image
               style={{width: size, height: size}}
               source={
                 require('./assets/setting-icon.png')
               }/>
             )
            }
          }}/>
      </Tab.Navigator>
    )
}
const DrawerNavigator = () =>{
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
        <Drawer.Screen name='HomeScreen' component={BottomNavigation}/>
    </Drawer.Navigator>
  )
}


