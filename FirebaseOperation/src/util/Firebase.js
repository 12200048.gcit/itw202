 import { firebase } from '@firebase/app'

// const CONFIG = {
//   apiKey: "AIzaSyAjCpcNaW76G8BQgOox64oLb2fSNLDc5A0",
//   authDomain: "firestore-operation.firebaseapp.com",
//   projectId: "firestore-operation",
//   storageBucket: "firestore-operation.appspot.com",
//   messagingSenderId: "797777204161",
//   appId: "1:797777204161:web:6614ad49e97cf614025540"
// };

const CONFIG = {
  apiKey: "AIzaSyAjCpcNaW76G8BQgOox64oLb2fSNLDc5A0",
  authDomain: "firestore-operation.firebaseapp.com",
  databaseURL: "https://firestore-operation-default-rtdb.firebaseio.com",
  projectId: "firestore-operation",
  storageBucket: "firestore-operation.appspot.com",
  messagingSenderId: "797777204161",
  appId: "1:797777204161:web:6614ad49e97cf614025540"
}
 firebase.initializeApp(CONFIG)

 export default firebase;
