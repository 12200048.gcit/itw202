import { StyleSheet, Text, View, Button } from 'react-native'
import React, {useState} from 'react'


const component = () => {
    const [count, setCount] = useState(0)
    const[disable, setDisable] = useState(false)
    const increase = () => {
        setCount(count+1)
        if(count>=2){
            setDisable(true)
        }
    }
  return (
    <View>
      <Text>you clicked {count} times</Text>

      <Button
      onPress={increase}
        title='Count'
        disabled={disable}
      />
    </View>
  )
}
export default component

const styles = StyleSheet.create({})