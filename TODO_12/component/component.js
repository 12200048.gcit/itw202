import { StyleSheet, Text, View, TextInput, Button, Pressable } from 'react-native'
import React, {useState} from 'react'


const component = () => {
    const [text, setText]= useState("")
    const [newText, setNewText]= useState([])
    const change = (proterty) =>{  
            setText(proterty)   
    }
    const cancel = () => {
        setText("")
    }
    const add = () => {
        if(text != ""){
            setNewText([...newText,text])
        }  
    }
  return (
    <View>
        <View>
            {newText.map(store=> <Text>{store}</Text>)}
        </View>

      <TextInput style = {styles.input}
      placeholder='Course Goal'
      onChangeText={change}
      value={text}
      />

      <View style={styles.buttons}> 
        <Pressable style={styles.p1} onPress={cancel} >
            <Text style={styles.t1}>CANCEL</Text>
        </Pressable>

        <Pressable onPress={add}>
            <Text style={styles.t2}>ADD</Text>
        </Pressable>
      </View>
      
    </View>
  )
}

export default component

const styles = StyleSheet.create({
    input: {
        borderWidth: 1,
        width: 300,
        height: 40,
        padding: 10
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginVertical: 10

    },
    p1: {
        marginHorizontal: 20,
    },
    t1: {
        color: 'red',
        fontWeight: 'bold'
    },
    t2: {
        color: 'blue',
        fontWeight: 'bold'
    }
})