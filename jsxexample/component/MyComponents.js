import React from 'react';
import {View, StyleSheet, Text } from "react-native";


const Mycomponent = () => {
    const greeting = 'sonam';
    const greeting1 = <Text>Jigme</Text>
    return(
        <View>
            <Text>hello</Text>
            <Text style = {styles.textStyle}>This is a demo of JSX</Text>
            <Text>Hi There!!! {greeting} </Text>
            {greeting1}
        </View>)
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
})
export default Mycomponent;