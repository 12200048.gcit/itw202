import { View, Text, StyleSheet, Alert, FlatList, useWindowDimensions } from 'react-native'
import React, {useState, useEffect} from 'react'
import Title from '../components/ui/Title'
import Colors from '../constants/Colors'
import NumberContainer from '../components/game/NumberContainer'
import PrimaryButton from '../components/ui/PrimaryButton'
import Card from '../components/ui/Card'
import InstructionText from '../components/ui/InstructionText'
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../components/game/GuessLogItem'


function generateRandomBetween(min, max, exclude){
  const rndNum = Math.floor(Math.random() * (max-min)) + min;

  if (rndNum == exclude){
    return generateRandomBetween(min, max, exclude)
  }
  else{
    return rndNum;
  }
 
}

let minBoundry = 1
let maxBoundry = 100
export default function GameScreen({userNumber, onGameOver}) {

  const initialGuess = generateRandomBetween(1, 100, userNumber)
  const [currentGuess, setCurrentGuess] = useState(initialGuess)
  const [guessRound, setGuessRound] = useState([initialGuess])
  const {width, height} = useWindowDimensions();
  useEffect(() => {
    if (currentGuess===userNumber){
        onGameOver(guessRound.length);
    }
  }, [currentGuess, userNumber, onGameOver])

  useEffect(()=>{
    minBoundry=1
    maxBoundry=100
  },[])


function nextGuessHandler(direction){
  if
    ((direction==='lower' && currentGuess<userNumber) || (direction==='greater' && currentGuess> userNumber))
    {
      Alert.alert('dont lie', 'you know its wrong', 
      [{text: 'sorry', style: 'cancel'}
      ])
      return;
    }
    if
    (direction==='lower'){
      maxBoundry = currentGuess;
   }
     else{
        minBoundry = currentGuess+1;
  }
  console.log(minBoundry, maxBoundry)
  const newRndNumber = generateRandomBetween(minBoundry, maxBoundry, currentGuess)
  setCurrentGuess(newRndNumber)
  setGuessRound((prevGuessRound => [newRndNumber, ...prevGuessRound]))
}

const guessRoundListLength = guessRound.length

let content = 
<>
<NumberContainer>{currentGuess}</NumberContainer>
    <Card>
        <InstructionText style={styles.instructiotext}>Higher or lower?</InstructionText>
            <View style={styles.buttonscontainer}>
              <View style={styles.buttoncontainer}> 
                <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                  <Ionicons name='md-add' size={24} color='white'/>
                </PrimaryButton>
              </View>
              
              <View style={styles.buttoncontainer}>
                <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                  <Ionicons name='md-remove' size={24} color='white'/>
                </PrimaryButton>
              </View>               
            </View>           
    </Card>
</>

if (width>500){
  content = (
    <>
      {/* <InstructionText style={styles.instructiotext}>Higher or lower?</InstructionText> */}
      <View style={styles.buttonscontainerwide}>
          <View style={styles.buttoncontainer}> 
            <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
              <Ionicons name='md-add' size={24} color='white'/>
            </PrimaryButton>
          </View>
          <NumberContainer>{currentGuess}</NumberContainer>        
          <View style={styles.buttoncontainer}>
            <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
              <Ionicons name='md-remove' size={24} color='white'/>
            </PrimaryButton>
          </View>               
      </View>  
  </>    
  )
}

return (
  <View style={styles.screen}>
    <Title>Opponent's Guess</Title>
    {content}
    <View style={styles.listcontainer}> 
        <FlatList
        data={guessRound}
        renderItem={(itemData)=>
            <GuessLogItem 
            roundNumber={guessRoundListLength - itemData.index}
            guess = {itemData.item}
            />
            }
            keyExtractor={(item) => item}
        />
        {/* {guessRound.map(guessRound=> <Text key={guessRound}>{guessRound}</Text>)} */}
    </View>
  </View>
)
}



const styles = StyleSheet.create({
    screen: {
        padding: 24,
        flex: 1,
        alignItems: 'center'

    },
    instructiotext: {
        marginBottom: 12,
    },
   buttoncontainer: {
     flex: 1
   },
    buttonscontainer:{
      flexDirection: 'row',
    },
     buttonscontainerwide:{
      flexDirection: 'row',
      alignItems: 'center'
     },
     listcontainer: {
       flex: 1,
       padding: 12
     }
})