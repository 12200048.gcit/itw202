import { View, Text, Image, StyleSheet, Dimensions, useWindowDimensions } from 'react-native'
import React from 'react'
import Title from '../components/ui/Title'
import Colors from '../constants/Colors'
import PrimaryButton from '../components/ui/PrimaryButton'
import { ScrollView } from 'react-native'
export default function GameOverScreen({roundNumber, userNumber, onStartNewGame}) {
  const {width, height} = useWindowDimensions();
  let imagesize = 300;
  if (width<380){
    imagesize = 150;
  }

  if (height< 400){
    imagesize = 80;
  }

  const imageStyle={
    width: imagesize,
    height: imagesize,
    borderRadius: imagesize/2
  }

  return (
    <ScrollView>
        <View style={styles.rootcontainer}>
        <Title>Game is over</Title>
        <View style={[styles.imagecontainer, imageStyle]}>
          <Image
          style={styles.image} 
          source={require("../assets/success.jpg")}/>
        </View>
        <Text style={styles.summarytext}> 
          Your phone needed  
            <Text style={styles.highlight}> {roundNumber} </Text>
          rounds to guess the number 
            <Text style={styles.highlight}> {userNumber} </Text>
        </Text>    
        <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>  
      </View>
    </ScrollView>
      
  )
}

const devicewidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  rootcontainer:{
    flex: 1,
    padding: 24,
    alignItems: 'center',
    justifyContent: 'center'
  },
   
  imagecontainer: {
    width: devicewidth <380 ? 150 : 300,
    height:devicewidth <380 ? 150: 300,
    borderRadius: devicewidth <380 ? 75: 150,
    borderWidth: 3,
    borderColor: Colors.primary800,
    overflow: 'hidden',
    margin: 36
  },
  image:{
    height: '100%',
    width: '100%'
  },

  summarytext:{
    fontFamily: 'OpenSans',
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 24
  },

  highlight: {
    // fontFamily: 
    color: Colors.primary500,
    fontWeight: 'bold'
  }
})
