import React, {useState} from 'react'
import { TextInput, View, Text, StyleSheet, Dimensions, height, useWindowDimensions } from 'react-native'
import { Alert } from 'react-native'
import Colors from '../constants/Colors'
import PrimaryButton from '../components/ui/PrimaryButton'
import Title from '../components/ui/Title'
import Card from '../components/ui/Card'
import InstructionText from '../components/ui/InstructionText'

import { KeyboardAvoidingView } from 'react-native'
import { ScrollView } from 'react-native'

export default function StartGameScreen({onPickNumber}) {
  const [enterNumber, setEnterNumber] = useState('')

  const {width, height} = useWindowDimensions()
 
  function numberInputHandler(enterText){
    setEnterNumber(enterText)
  }
  
  function resetInputHandler(){
    setEnterNumber("")
  }

  function confirmInputHandler(){
    const chosenNumber = parseInt(enterNumber)

    if(isNaN(chosenNumber) || chosenNumber<1 || chosenNumber > 99){
      Alert.alert('invalid Number','Number has to be between 1 tp 99',
      [{text:'okay', style: 'destructive', onPress: resetInputHandler}]
    )
    return;
    }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)
  }

  const marginTopDistance = height < 380 ? 30: 100;
  
  return (
    <ScrollView style={styles.screen}>
        <KeyboardAvoidingView style={styles.screen} behavior="position">
          <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
          <Title>Guess My Number</Title>
            <Card style={styles.Inputcontainer}>
              <InstructionText style={styles.Instructiontext}>Enter a number</InstructionText>
              <TextInput style={styles.numberInput} 
                  maxLength={2} 
                  keyboardType={'number-pad'}
                  autoCapitalize='none'
                  autoCorrect={false}
                  value={enterNumber}
                  onChangeText={numberInputHandler}/>

                    <View style={styles.buttonsContainer}>
                  
                        <View style={styles.buttonContainer}>
                            <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
                        </View>

                          <View style={styles.buttonContainer}>
                              <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
                          </View>
                    </View>
            </Card>
          </View>
      </KeyboardAvoidingView>
    </ScrollView>

       
  )
}

//const deviceHeight = Dimensions.get('window').height

const styles = StyleSheet.create({

  screen:{
    flex: 1
  },

  rootContainer: {
      //flex: 1,
      //marginTop: deviceHeight < 380 ? 30: 100,
      alignItems: 'center'
  },
  Instructiontext: {
      color: Colors.accent500,
      fontSize: 24
  },
  Inputcontainer:  {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 36,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: Colors.primary500,
    borderRadius: 8,
    elevation: 4,
    // shadowColor: 'black',
    // shadowOffset: {width: 0, height: 2},
    // shadowRadius: 6,
    // shadowOpacity: 0.25
  },
  numberInput: {

    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:Colors.primary900,
    borderBottomWidth: 2,
    color: Colors.primary900,
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  buttonsContainer: {
    flexDirection: 'row'
},
buttonContainer: {
    flex: 1,
}
})