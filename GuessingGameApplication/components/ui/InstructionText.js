import { View, Text, StyleSheet } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'

export default function InstructionText({children, style }) {
  return (
      <Text style={[styles.instructiontext, style]}>{children}</Text>
  )
}

const styles=StyleSheet.create({
    instructiontext:{
        color: Colors.accent500,
        fontSize: 24,
        fontFamily: 'open-sans'
    }
})