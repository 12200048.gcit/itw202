import { View, Text, StyleSheet, Dimensions } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'


export default function Card({children}) {
  return <View style={styles.card}>{children}</View>
}
const devicewidth = Dimensions.get('window').width

const styles=StyleSheet.create({
    card:{
      justifyContent: 'center',
      alignItems:'center',
      marginTop: devicewidth < 380 ? 18: 36,
      marginHorizontal: 24,
      padding: 16,
      backgroundColor: Colors.primary500,
      borderRadius: 8,
      elevation: 8,
        // shadowColor: 'black',
        // shadowOffset: {width: 0, height: 2},
        // shadowRadius: 6,
        // shadowOpacity: 0.25
    }
})