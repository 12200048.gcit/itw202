import React from 'react'
import {View, Text, StyleSheet, Pressable} from 'react-native'
import Colors from '../../constants/Colors'


export default function PrimaryButton({children, onPress}) {
  return (
    <View style={styles.buttonOuterContainer}>
        <Pressable android_ripple={{color: Colors.primary600 }}
        style={({pressed}) => pressed ?
        [styles.buttoninnerContainer, styles.pressed]:
        styles.buttoninnerContainer}
        onPress={onPress}>

            <Text style={styles.buttonText}> {children} </Text>
            
        </Pressable>
    </View>
  )   
}

const styles = StyleSheet.create({
    buttonOuterContainer: {
        borderRadius: 28,
        margin: 4,
        overflow: 'hidden'
    },
    buttoninnerContainer: {
        backgroundColor: Colors.primary500,
        borderRadius: 28,
        paddingVertical: 8,
        paddingHorizontal: 16,
        elevation: 2,
        margin: 4,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center'
    },
    pressed: {
        opacity: 0.75
    }
})