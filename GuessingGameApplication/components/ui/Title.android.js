import { StyleSheet, Text, View, Platform } from 'react-native'
import React from 'react'
import Colors from '../../constants/Colors'

const Title = ({children}) => {
  return (
    <View>
        <Text style={styles.t1}>{children}</Text>
    </View> 
    )
}

export default Title

const styles = StyleSheet.create({   
    t1: {
        // borderWidth: 2,
        borderWidth: Platform.OS === 'android' ? 2 : 0,
        fontFamily: 'open-sans-bold',
        borderColor: 'white',
        textAlign: 'center',
        color: 'white',
        padding: 12,
        fontSize: 24,
        maxWidth: '80%',
        width: 300
        
    }
})