import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, SafeAreaView, ImageBackground } from 'react-native';
import StartGameScreen from './Screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import { useState } from 'react';
import GameScreen from './Screens/GameScreen';
import Colors from './constants/Colors';
import GameOverScreen from './Screens/GameOverScreen';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';



export default function App() {
  const [userNumber, setUserNumber] = useState()
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRound, setGuessRound] = useState(0)

const [fontsLoaded] = useFonts({
  'open-sans': require('./assets/fonts/OpenSans-Bold.ttf'),
  'open-sans-bold': require('./assets/fonts/OpenSans-Regular.ttf'),
})
if(!fontsLoaded){
  return <AppLoading/>
}

  function pickedNumberHandler (pickerNumber){
    setUserNumber(pickerNumber)
       setGameIsOver(false)
  }

  function gameOverHandler(numberOfRounds){
    setGameIsOver(true)
    setGuessRound(numberOfRounds)
  }

  function startNewGameHandler(){
    setUserNumber(null);
    setGuessRound(0)
  }
  
  let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>
  
  if(userNumber){
    screen=<GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
  }
  if(gameIsOver && userNumber){
    screen = <GameOverScreen
               userNumber={userNumber}
               roundNumber={guessRound}
               onStartNewGame={startNewGameHandler}
               />
  }
  return (
    <>
    <StatusBar style='light'/> 
      <LinearGradient style={styles.rootScreen} colors={[Colors.primary700, Colors.accent500]}>
      <ImageBackground
      source={require('./assets/dd.jpg')}
      resizeMode="cover"
      style={styles.rootScreen}
      imageStyle={styles.backgroundImage}
      >
        {screen}
      </ImageBackground>
      <StatusBar style="auto" />
      {/* <SafeAreaView style={styles.rootScreen}>
         {screen} 
      </SafeAreaView>  */}
    </LinearGradient>
    </>
    
  );
}

const styles = StyleSheet.create({
  rootScreen: {
    flex: 1,
  },
  backgroundImage: {
    opacity: 0.25
  }
});
