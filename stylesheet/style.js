import React from 'react';
import { StyleSheet, Text, TextInput, Button } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#830A7B'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    }

});
export default styles;