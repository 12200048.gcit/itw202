import { StyleSheet, Text, View, Button} from 'react-native';
import React, {useState} from 'react'

const component = () => {
    const [count, setCount] = useState(0)
  return (
    <View>
      <Text>you have click {count} times </Text>
      <Button
      onPress={() => setCount(count+1)}
      title= 'count'
      />
    </View>
    
  )
}

export default component

const styles = StyleSheet.create({})