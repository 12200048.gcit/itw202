import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import TextInp from './component/component';
export default function App() {
  return (
    <View style={styles.container}>
      <TextInp
      />
      <StatusBar style="auto" />
    </View>
  );
}

