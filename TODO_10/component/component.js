import React from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native';
import { useState } from 'react';

export default function TextInp(){
    const [text, setText] = useState('what is your name');
    return(
        <View style = {Styles.container}>
        <Text>{text}</Text>

        <TextInput
        onChangeText={inputValue=> setText(<Text>Hi {inputValue} from Gyalpozhing college of information technology</Text>)}
        style = {styles.in}/>
        </View>
    )

    
}
const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    
    },

    in: {
        borderWidth: 1,
        borderColor: 'black',
        height: 30,
        weight: 300
    }
})