import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Component from './component/conponent';
export default function App() {
  return (
    <View style = {styles.container}>
       <Component/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    marginBottom: 100,
    fontSize: 16 
  },
});
