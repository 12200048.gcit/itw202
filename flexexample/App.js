import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    //<>
     //<View style={styles.container}></View>    //1
    // <View style={styles.style1}View/>  //2
    //<View style={styles.style2}View/>   //2
    //</>


    <View style = {styles.container}>  //3
        <View style={styles.square}/>
        <View style={styles.square}/>
        <View style={styles.square}/>
    </View>
  );
}

// const styles = StyleSheet.create({ //1
//   container: {
//     flex: 1,
//     backgroundColor: '#D242CD',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });



// const styles = StyleSheet.create({ //2
//   style1: {
//     flex: 1,
//     backgroundColor: '#CAC139',
//     alignContent: 'center',
//     alignItems: 'center'
//   },

//   style2: {
//     flex: 3,
//     backgroundColor: '#D35400',
//     alignContent: 'center',
//     justifyContent: 'center'
//   }
// })


const styles = StyleSheet.create({  //3
  container: {
    backgroundColor: '#D35400',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    flexDirection: "column"
 },
 square: {
   backgroundColor: '#8E44AD',
   borderWidth: 3,
   borderColor: 'black',
   width: 100,
   height: 100,
   margin: 4,
 },
});