import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './Components/classComponent';

export default class App extends React.classComponent  {
  render = () => (
    <View style={styles.container}>
      <Text>practical2</Text>
      <Text>Stateless and Statefull components</Text>
      <Text style={styles.text}>
        You are ready to start the journey.
        
      </Text>
     
      <StatusBar style="auto" />
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
