import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const name = 'Jamyang Gyeltshen'
  return (
    <View style={styles.container}>
      <Text style={styles.s1}>Getting started with react native!</Text>
      <Text style={styles.s2}>My name is {name}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  s1: {
    fontSize: 45,
    textAlign: 'center'
  },

  s2: {
    fontSize: 20
  }



});
