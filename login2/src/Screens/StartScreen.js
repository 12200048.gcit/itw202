import React from 'react'
import {View, StyleSheet} from 'react-native'
import Paragraph from '../components/Paragraph'
import Button from '../components/Button'
import Header from '../components/Header'
import Background from '../components/Background'
import Logo from '../components/Logo'


export default function StartScreen({navigation}){
    return(
        <Background>
            <Logo/>
            <Header>Login Page</Header>
            <Paragraph>
                The easiest way to start with your amazing application
            </Paragraph>
            <Button
             mode="outlined"
             onPress = {() =>{
                 navigation.navigate("LoginScreen")
             }}
            >LOGIN</Button>

            <Button
            mode="contained"
             onPress = {() =>{
                 navigation.navigate("RegistrationScreen")
             }}
            >Sign UP</Button>
           
        </Background>
    )
}

