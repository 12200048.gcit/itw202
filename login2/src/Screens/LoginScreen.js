import React, {useState} from 'react'
import {View, StyleSheet} from 'react-native'
import Paragraph from '../components/Paragraph'
import Button from '../components/Button'
import Header from '../components/Header'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/Textinput'
import BackButton from '../components/BackButton'

export default function LoginScreen({navigation}){
    const [email, setEmail]=useState({value:"", error:""})
    const [password, setPassword]=useState({value:'', error:''})

    
    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>welcome</Header>
            <TextInput label='Email'
            value={email.value}
            error={email.error}
            errorText={email.error}
            onChangeText={(text)=>setEmail({value:text, error:""})}/>

            <TextInput
             label='Password'
            value={password.value}
            error={password.error}
            errorText={password.error}
            onChangeText={(text)=>setPassword({value:text, error:''})}
            secureTextEntry
            />
            <Button mode='contained'>LOGIN</Button>
        </Background>
    )
}

