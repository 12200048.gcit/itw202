import React, {useState} from 'react'
import {View, StyleSheet} from 'react-native'
import Paragraph from '../components/Paragraph'
import Button from '../components/Button'
import Header from '../components/Header'
import Background from '../components/Background'
import Logo from '../components/Logo'
import TextInput from '../components/Textinput'
import BackButton from '../components/BackButton'


export default function RegistrationScreen({navigation}){
    const [email, setEmail]=useState({value:"", error:""})
    const [password, setPassword]=useState({value:'', error:''})
    


    return(
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create Account</Header>
            {/* <TextInput label='Name'
            value={name.value}
            error={name.error}
            errorText={name.error}
            onChangeText={(text)=>setName({value:text, error:""})}/> */}

            <TextInput label='Email'
            value={email.value}
            error={email.error}
            errorText={email.error}
            onChangeText={(text)=>setEmail({value:text, error:""})}/>

            <TextInput
            label='Password'
            value={password.value}
            error={password.error}
            errorText={password.error}
            onChangeText={(text)=>setPassword({value:text, error:''})}
            secureTextEntry
            />
            <Button mode='contained'>SIGN UP</Button>
            {/* <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity onPress={() => navigation.replace("LoginScreen")}>
                    <Text style={styles.link}>Login</Text>
                </TouchableOpacity>
            </View> */}
        </Background>
    )
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary
    }
})

