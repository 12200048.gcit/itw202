import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/Button'
import TextInput from './src/components/Textinput';
import Header from './src/components/Header';
import Paragraph from './src/components/Paragraph';
import {NavigationContainer} from '@react-navigation/native'
import { LoginScreen,
   StartScreen,
    RegistrationScreen}
     from './src/Screens';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack =createNativeStackNavigator()

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
        initialRouteName='RegistrationScreen'
        screenOptions={{headerShown: false}}>

          <Stack.Screen name='StartScreen' component={StartScreen}/>
          <Stack.Screen name='LoginScreen' component = {LoginScreen}/>
          <Stack.Screen name='RegistrationScreen' component = {RegistrationScreen}/>

        </Stack.Navigator>
      </NavigationContainer>

      {/* <View style={styles.container}>
        <Header>open</Header>
        <Button mode='contained'>Click Me</Button>
        <TextInput label = 'email'/>
      </View> */}
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
